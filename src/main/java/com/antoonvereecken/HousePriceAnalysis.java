package com.antoonvereecken;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.*;
import org.apache.spark.ml.evaluation.RegressionEvaluator;
import org.apache.spark.ml.feature.OneHotEncoder;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.param.ParamMap;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.ml.tuning.ParamGridBuilder;
import org.apache.spark.ml.tuning.TrainValidationSplit;
import org.apache.spark.ml.tuning.TrainValidationSplitModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;
import static org.apache.spark.sql.functions.log;

public class HousePriceAnalysis {
    static void logInfo(Object o) {
        Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o);
    }

    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        SparkSession spark = SparkSession.builder()
                .master("local[*]").appName("HousePriceAnalysisApp")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .getOrCreate();
        Dataset<Row> dataset = spark.read()
                .option("header", true)
                .option("inferSchema", true)
                .csv("src/main/resources/kc_house_data.csv")
                .drop("long", "lat");

        dataset = dataset
                .withColumnRenamed("price", "label")
                .withColumn("sqft_above_percentage",
                        col("sqft_above").divide(col("sqft_living")));

        Dataset<Row>[] datasetRandomSplit = dataset.randomSplit(new double[]{ 0.8, 0.2 });
        Dataset<Row> trainingAndTestData = datasetRandomSplit[0];
        Dataset<Row> holdOutData = datasetRandomSplit[1];

        StringIndexer stringIndexer = new StringIndexer()
                .setInputCols(new String[] {"condition", "grade", "zipcode"})
                .setOutputCols(new String[] {"conditionIndexed", "gradeIndexed", "zipcodeIndexed"});
        OneHotEncoder hotEncoder = new OneHotEncoder()
                .setInputCols(new String[] {"conditionIndexed", "gradeIndexed", "zipcodeIndexed"})
                .setOutputCols(new String[] {"conditionVector", "gradeVector", "zipcodeVector"});
        VectorAssembler featuresVector = new VectorAssembler()
                .setInputCols(new String[]{
                    "bedrooms","bathrooms","sqft_living","sqft_above_percentage", "floors",
                    "conditionVector", "gradeVector","zipcodeVector","waterfront"
                })
                .setOutputCol("features");
        VectorAssembler differentFeaturesVector = new VectorAssembler()
                .setInputCols(new String[]{
                        "sqft_living","sqft_above_percentage", "floors",
                        "conditionVector", "gradeVector","zipcodeVector","waterfront"
                })
                .setOutputCol("features");

        LinearRegression linearRegression = new LinearRegression();
        ParamGridBuilder paramGridBuilder = new ParamGridBuilder();
        ParamMap[] paramMaps = paramGridBuilder
                .addGrid( linearRegression.regParam(), new double[]{0.01, 0.02, 0.03} )
                .addGrid( linearRegression.elasticNetParam(), new double[]{0.3, 0.4, 0.5, 0.6} )
                .build();
        TrainValidationSplit trainValidationSplit = new TrainValidationSplit()
                .setEstimator(linearRegression)
                .setEvaluator( new RegressionEvaluator().setMetricName("r2") )
                .setEstimatorParamMaps(paramMaps)
                .setTrainRatio(0.8);

        Pipeline pipeline = new Pipeline().setStages(
                new PipelineStage[]{stringIndexer,hotEncoder,featuresVector,trainValidationSplit}
        );
        PipelineModel pipelineModel = pipeline.fit(trainingAndTestData);
        TrainValidationSplitModel model = (TrainValidationSplitModel)pipelineModel.stages()[3];
        LinearRegressionModel lrModel = (LinearRegressionModel)	model.bestModel();

        Dataset<Row> holdOutResults = pipelineModel.transform(holdOutData);
        holdOutResults.show();
        holdOutResults = holdOutResults.drop("prediction"); // need to drop so we can lrModel.evaluate

        logInfo("FEATURE_COEFFICIENTS = " +lrModel.coefficients());
        logInfo("INTERCEPT = " +lrModel.intercept());
        logInfo("------------------------------TRAINING-DATA--------------------------------------");
        logInfo("RMSE = " +lrModel.summary().rootMeanSquaredError());
        logInfo("R² = " +lrModel.summary().r2());
        logInfo("--------------------------------TEST-DATA----------------------------------------");
        logInfo("RMSE = " +lrModel.evaluate(holdOutResults).rootMeanSquaredError());
        logInfo("R² = " +lrModel.evaluate(holdOutResults).r2());
        logInfo("----------------------------------PARAMS-----------------------------------------");
        logInfo("REG_PARAM = " +lrModel.getRegParam());
        logInfo("ELASTIC_NET_PARAM = " + lrModel.getElasticNetParam());


        logInfo("SHUTTING DOWN");
        spark.close();
    }
}
