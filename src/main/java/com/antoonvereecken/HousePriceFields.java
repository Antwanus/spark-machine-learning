package com.antoonvereecken;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class HousePriceFields {
    static void logInfo(Object o) {
        Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o);
    }
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        SparkSession spark = SparkSession.builder()
                .master("local[*]").appName("HousePriceAnalysisApp")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .getOrCreate();
        Dataset<Row> dataset = spark.read()
                .option("header", true)
                .option("inferSchema", true)    // auto-cast type
                .csv("src/main/resources/kc_house_data.csv");
//        dataset.printSchema();dataset.show(5);logInfo("dataset.describe().show()");
        /**id
         * date             We're working in a dataset that spans over a year -> no need
         * price            =label
         * bedrooms         amount
         * bathrooms        .
         * sqft_living      .
         * sqft_lot         .
         * floors           .
         * waterfront       0 or 1 (= boolean, chapter 40: Logistic Regression)
         * view             how many times the house has been visited by potential buyers
         * condition        how good the condition is               min:1, max:5
         * grade            based on King County grading system     min:1, max:13
         * sqft_above
         * sqft_basement
         * yr_built
         * yr_renovated     lot of empties (0) -> not sure
         * zipcode          -> can't use it yet (later chaper
         * lat              I 'm not sure if this value is useful on it's own
         * long             Maybe combined latitude & longitude is?
         * sqft_living15    size as of 2015?
         * sqft_lot15       .
         */
        dataset=dataset.drop("id","date","waterfront","view","condition","grade","yr_renovated","zipcode","lat","long");
        logInfo("I. filter 'interesting' columns before calculating correlation:");
        dataset.show(5);
        for (String col : dataset.columns())
            logInfo("correlation(price,"+col+") = " + dataset.stat().corr("price", col));

        dataset=dataset.drop("sqft_living15", "sqft_lot15", "yr_built");
        logInfo("II. filter 'interesting' columns, calculating correlation between each other:");
        dataset.show(5);
        for (String col1 : dataset.columns()){
            for (String col2 : dataset.columns()) {
                logInfo("correlation("+col1+","+col2+") = " + dataset.stat().corr(col1, col2));
            }
        }



        spark.close();
    }
}
