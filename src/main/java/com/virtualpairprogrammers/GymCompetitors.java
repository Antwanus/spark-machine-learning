package com.virtualpairprogrammers;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.spark.ml.feature.OneHotEncoder;
import org.apache.spark.ml.feature.StringIndexer;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.regression.LinearRegression;
import org.apache.spark.ml.regression.LinearRegressionModel;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import static org.apache.spark.sql.functions.col;

public class GymCompetitors {
    static void logInfo(Object o) {
        Logger.getLogger("com.sparksql").info("*** My Log ***\t-->\t" + o);
    }
    public static void main(String[] args) {
        Logger.getLogger("org.apache").setLevel(Level.WARN);
        System.setProperty("hadoop.home.dir", "c:/hadoop");
        SparkSession spark = SparkSession.builder()
                .master("local[*]").appName("GymCompetitors")
                .config("spark.sql.warehouse.dir", "file:///c:/tmp/")
                .getOrCreate();
        Dataset<Row> dataset = spark.read()
                .option("header", true)
                .option("inferSchema", true)    // auto-cast type
                .csv("src/main/resources/GymCompetition.csv");

//        dataset.show(5); dataset.printSchema();

        StringIndexer genderIndexer = new StringIndexer()
                .setInputCol("Gender")
                .setOutputCol("GenderIndexed");
        dataset = genderIndexer.fit(dataset).transform(dataset);
        OneHotEncoder genderEncoder = new OneHotEncoder()
               .setInputCols(new String[]{"GenderIndexed"})
               .setOutputCols(new String[]{"GenderVector"});
        dataset = genderEncoder.fit(dataset).transform(dataset);
        dataset.show(10);


        VectorAssembler vectorAss = new VectorAssembler()
                                .setInputCols(new String[]{"Height", "Weight", "Age"})
                                .setOutputCol("features");
        Dataset<Row> datasetWithFeatures = vectorAss.transform(dataset);
        Dataset<Row> modelInputData = datasetWithFeatures
                .select(
                        col("NoOfReps").alias("label"),
                        col("features")
                );
//        modelInputData.show(); modelInputData.printSchema();


        LinearRegression linearRegression = new LinearRegression();
        LinearRegressionModel linRegModel = linearRegression.fit(modelInputData);
        logInfo("interceptor_value: " + linRegModel.intercept());
        logInfo("coefficients_values: " + linRegModel.coefficients());
        linRegModel.transform(modelInputData).show();


        spark.close();
    }
}
